#include <stdio.h>

int main(void) {    
int inp, reverse=0, rem;    
printf("Enter a number: ");    
  scanf("%d", &inp);    
  while(inp!=0)    
  {    
     rem=inp%10;    
     reverse=reverse*10+rem;    
     inp/=10;    
  }    
  printf("Reversed Number: %d",reverse);    
return 0;  
}  