#include <stdio.h>

int main(void) {    
  int inp;

  printf("Enter an integer: ");
  scanf("%d",&inp);

  for(int i = 1;i<=inp;i++){
    int mul = inp*i;
    printf("%d * %d = %d \n", inp,i,mul);
  }
  return 0;
}  
