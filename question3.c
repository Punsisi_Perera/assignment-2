#include <stdio.h>

int main(void) {
  
  char letter;

  printf("Enter the letter: ");
  scanf(" %c",&letter);

  if(letter=='a' || letter=='A')
  {
    printf("%c is a vowel.",letter);
  }
  else if(letter=='e' || letter=='E')
  {
    printf(" %c is a vowel.",letter);
  }
  else if(letter=='i' || letter=='I')
  {
    printf(" %c is a vowel.",letter);
  }
  else if(letter=='o' || letter=='O')
  {
    printf(" %c is a vowel.",letter);
  }
  else if(letter=='u' || letter=='U')
  {
    printf(" %c is a vowel.",letter);
  }
  else
  {
    printf("The letter %c is a consonant ",letter);
  }
  return 0;
}
