#include <stdio.h>

int main(void)
{
    int input;
    
    printf("Enter the number:");
    scanf("%d",&input);
    
    if(input>0)
    {
        printf("The entered number is a POSITIVE");
    }
    else if(input<0)
    {
        printf("The entered number is a NEGATIVE");
    }
    else
    {
        printf("The entered number is 0");
    }

    return 0;
}
